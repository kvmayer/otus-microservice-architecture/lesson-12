# otus-lesson-12-homework

## Архитектура

В качестве api-gateway используется [traefik](https://doc.traefik.io/traefik/v2.0/). Для аутентификации используется [forward-auth middleware](https://doc.traefik.io/traefik/v2.0/middlewares/forwardauth/).

![img.png](./assets/img.png)

## Зависимости

- Minikube 1.20.0
- Kubectl 0.19.2
- Helm 3.3.4

## Установка

Собрать образ:
```shell
eval $(minikube docker-env) && docker build -t auth-service:latest -f auth/Dockerfile auth
```

Развернуть сервис:
```shell
kubectl apply -f k8s.yaml
```

Создать неймспейс для traefik:
```shell
kubectl create namespace traefik
```

Установка traefik из helm:
```shell
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
helm install --version "10.1.2" -n traefik --values traefik-values.yaml traefik traefik/traefik
```

Настройка маршрутизации:
```shell
kubectl apply -f traefik.yaml
```

## Тесты

Запуск:
```shell
 newman run otus-lesson-12-homework.postman_collection.json --verbose
```

Если кластер не резолвится по arch.homework:
```shell
newman run otus-lesson-12-homework.postman_collection.json --verbose --env-var baseUrl=$(minikube ip)
```

Результат тестов
```shell
otus-lesson-12-homework

→ регистрация пользователя 1
  POST arch.homework/auth/sign-up  
  302 Found ★ 71ms time ★ 370B↑ 103B↓ size ★ 8↑ 3↓ headers ★ 0 cookies
  ┌ ↑ urlencoded ★ 111B
  │ login=Ruby.Padberg@gmail.com&password=9k7NX1TVK61I_F5&retry_password=9k7NX1TVK61I_F5&name=Harry&surname=Harry
  └ 
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait   dns-lookup   tcp-handshake   transfer-start   download   process   total 
  24ms      3ms    340µs        602µs           61ms             4ms        242µs     95ms  

  ✓  status must be 302
  ✓  header location must be /auth/sign-in

→ проверка, что изменение и получение профиля пользователя недоступно без логина
  GET arch.homework/users//profile  
  401 Unauthorized ★ 4ms time ★ 239B↑ 85B↓ size ★ 7↑ 2↓ headers ★ 0 cookies
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  1ms       535µs   (cache)      (cache)         1ms              1ms        59µs      4ms   

  ✓  status must be 401

→ вход пользователя 1
  POST arch.homework/auth/sign-in  
  302 Found ★ 69ms time ★ 313B↑ 1.33kB↓ size ★ 8↑ 5↓ headers ★ 0 cookies
  ┌ ↑ urlencoded ★ 55B
  │ login=Ruby.Padberg@gmail.com&password=9k7NX1TVK61I_F5
  └ 
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  1ms       292µs   (cache)      (cache)         65ms             2ms        46µs      70ms  

  ✓  status code is 302
  ✓  location is /
  ✓  has cookie access-token
  ✓  has cookie refresh-token

→ проверка регистрации пользователь 1
  GET arch.homework/auth/check  
  200 OK ★ 3ms time ★ 1.42kB↑ 716B↓ size ★ 8↑ 5↓ headers ★ 0 cookies
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  1ms       234µs   (cache)      (cache)         1ms              1ms        31µs      3ms   

  ✓  response has header x-user-id
  ✓  response has header x-auth-token
  ✓  response has header x-username
  ✓  heder x-username must be equal first user login

→ изменение профиля пользователя 1
  PATCH arch.homework/users/82/profile  
  200 OK ★ 4ms time ★ 1.53kB↑ 75B↓ size ★ 10↑ 2↓ headers ★ 0 cookies
  ┌ ↑ raw ★ 47B
  │ {
  │     "name": "Wayne",
  │     "surname": "Wayne"
  │ }
  └ 
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  1ms       358µs   (cache)      (cache)         1ms              1ms        33µs      4ms   

  ✓  response status 200

→ проверка, что профиль поменялся
  GET arch.homework/users/82/profile  
  200 OK ★ 3ms time ★ 1.43kB↑ 198B↓ size ★ 8↑ 3↓ headers ★ 0 cookies
  ┌ ↓ application/json ★ text ★ json ★ utf8 ★ 75B
  │ {"Id":82,"Name":"Wayne","Surname":"Wayne","Login":"Ruby.Padberg@gmail.com"}
  └
  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  950µs     225µs   (cache)      (cache)         1ms              1ms        141µs     4ms   

  ✓  chech new username

→ регистрация пользователя 2
  POST arch.homework/auth/sign-up  
  302 Found ★ 63ms time ★ 1.56kB↑ 103B↓ size ★ 9↑ 3↓ headers ★ 0 cookies
  ┌ ↑ urlencoded ★ 111B
  │ login=Marcella44@yahoo.com&password=qLyGM4MFa3tzcXV&retry_password=qLyGM4MFa3tzcXV&name=Amparo&surname=Amparo
  └ 
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  1ms       278µs   (cache)      (cache)         61ms             1ms        32µs      63ms  

  ✓  status must be 302
  ✓  header location must be /auth/sign-in

→ вход пользователя 2
  POST arch.homework/auth/sign-in  
  302 Found ★ 67ms time ★ 1.5kB↑ 1.32kB↓ size ★ 9↑ 5↓ headers ★ 0 cookies
  ┌ ↑ urlencoded ★ 53B
  │ login=Marcella44@yahoo.com&password=qLyGM4MFa3tzcXV
  └ 
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  975µs     255µs   (cache)      (cache)         64ms             1ms        20µs      67ms  

  ✓  status code is 302
  ✓  location is /
  ✓  has cookie access-token
  ✓  has cookie refresh-token

→ проверка, что пользователь2 не имеет доступа на чтение и редактирование профиля пользователя1
  GET arch.homework/users/82/profile  
  403 Forbidden ★ 3ms time ★ 1.42kB↑ 82B↓ size ★ 8↑ 2↓ headers ★ 0 cookies
  ↓ text/plain ★ text ★ plain ★ utf8

  prepare   wait    dns-lookup   tcp-handshake   transfer-start   download   process   total 
  933µs     199µs   (cache)      (cache)         1ms              1ms        22µs      3ms   

  ✓  response code must be 403

┌─────────────────────────┬─────────────────────┬────────────────────┐
│                         │            executed │             failed │
├─────────────────────────┼─────────────────────┼────────────────────┤
│              iterations │                   1 │                  0 │
├─────────────────────────┼─────────────────────┼────────────────────┤
│                requests │                   9 │                  0 │
├─────────────────────────┼─────────────────────┼────────────────────┤
│            test-scripts │                  18 │                  0 │
├─────────────────────────┼─────────────────────┼────────────────────┤
│      prerequest-scripts │                  13 │                  0 │
├─────────────────────────┼─────────────────────┼────────────────────┤
│              assertions │                  20 │                  0 │
├─────────────────────────┴─────────────────────┴────────────────────┤
│ total run duration: 585ms                                          │
├────────────────────────────────────────────────────────────────────┤
│ total data received: 75B (approx)                                  │
├────────────────────────────────────────────────────────────────────┤
│ average response time: 31ms [min: 3ms, max: 71ms, s.d.: 31ms]      │
├────────────────────────────────────────────────────────────────────┤
│ average DNS lookup time: 340µs [min: 340µs, max: 340µs, s.d.: 0µs] │
├────────────────────────────────────────────────────────────────────┤
│ average first byte time: 28ms [min: 1ms, max: 65ms, s.d.: 30ms]    │
└────────────────────────────────────────────────────────────────────┘
```