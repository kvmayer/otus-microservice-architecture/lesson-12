package domain

type User struct {
	Id          int
	Name        string
	Surname     string
	Credentials Credentials
}

type Credentials struct {
	Login    string
	Password string
}

func CreateNewUser(id int, name, surname, login, password string, encoder PasswordEncoder) *User {
	return &User{
		Id:      id,
		Name:    name,
		Surname: surname,
		Credentials: Credentials{
			Login:    login,
			Password: encoder.Encode(password),
		},
	}
}

func (u *User) ValidatePassword(password string, encoder PasswordEncoder) bool {
	return encoder.ComparePassword(u.Credentials.Password, password)
}

func (u *User) Login() string {
	return u.Credentials.Login
}
