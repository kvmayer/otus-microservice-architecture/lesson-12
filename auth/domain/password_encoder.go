package domain

type PasswordEncoder interface {
	Encode(password string) string
	ComparePassword(hash, password string) bool
}
