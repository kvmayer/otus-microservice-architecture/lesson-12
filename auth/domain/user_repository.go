package domain

type UserRepository interface {
	Save(user *User) error
	FindByLogin(login string) (*User, error)
	FindById(id int) (*User, error)
	NextId() int
}
