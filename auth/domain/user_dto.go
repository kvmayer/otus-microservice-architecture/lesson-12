package domain

type SignInDto struct {
	Login    string `json:"login" form:"login"`
	Password string `json:"password" form:"password"`
}

type SignUpDto struct {
	Name          string `json:"name" form:"name"`
	Surname       string `json:"surname" form:"surname"`
	Login         string `json:"login" form:"login"`
	Password      string `json:"password" form:"password"`
	RetryPassword string `json:"retry_password" form:"retry_password"`
}

type GetOwnedProfileDto struct {
	Id int `json:"id" uri:"id"`
}

type UpdateOwnedProfile struct {
	Id      int    `json:"id" uri:"id"`
	Name    string `json:"name" form:"name"`
	Surname string `json:"surname" form:"name"`
}
