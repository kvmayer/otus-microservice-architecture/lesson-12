package domain

import "github.com/golang-jwt/jwt"

type AuthToken struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type CustomClaim struct {
	UserId int `json:"user_id"`
	jwt.StandardClaims
}
