package domain

import (
	"errors"
)

var (
	InvalidRetryPassword = errors.New("invalid retry password")
	UserNotExists        = errors.New("user not exists")
	InvalidPassword      = errors.New("invalid password")
	UserAlreadyExists    = errors.New("user already exists")
	PermissionDeny       = errors.New("permission deny")
)

type UserService struct {
	userRepository  UserRepository
	passwordEncoder PasswordEncoder
	tokenService    *AuthTokenService
}

func NewUserService(userRepository UserRepository, passwordEncoder PasswordEncoder, tokenService *AuthTokenService) *UserService {
	return &UserService{
		userRepository:  userRepository,
		passwordEncoder: passwordEncoder,
		tokenService:    tokenService,
	}
}

func (s *UserService) UserSignIn(dto *SignInDto) (*AuthToken, error) {
	user, err := s.userRepository.FindByLogin(dto.Login)
	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, UserNotExists
	}

	if !user.ValidatePassword(dto.Password, s.passwordEncoder) {
		return nil, InvalidPassword
	}

	return s.tokenService.GenerateAuthToken(user), nil
}

func (s *UserService) UserSingUp(dto *SignUpDto) error {
	if dto.Password == "" {
		return InvalidPassword
	}

	if dto.Password != dto.RetryPassword {
		return InvalidRetryPassword
	}

	if user, err := s.userRepository.FindByLogin(dto.Login); err != nil {
		return err
	} else if user != nil {
		return UserAlreadyExists
	}

	user := CreateNewUser(s.userRepository.NextId(), dto.Name, dto.Surname, dto.Login, dto.Password, s.passwordEncoder)

	return s.userRepository.Save(user)
}

func (s *UserService) GetOwnedProfile(ctx RequestContext, dto *GetOwnedProfileDto) (*User, error) {
	if ctx.UserId != dto.Id {
		return nil, PermissionDeny
	}

	user, err := s.userRepository.FindById(dto.Id)
	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, UserNotExists
	}

	return user, nil
}

func (s *UserService) UpdateOwnedUserName(ctx RequestContext, dto *UpdateOwnedProfile) error {
	if ctx.UserId != dto.Id {
		return PermissionDeny
	}

	user, err := s.userRepository.FindById(dto.Id)
	if err != nil {
		return err
	}

	if user == nil {
		return UserNotExists
	}

	user.Name = dto.Name
	user.Surname = dto.Surname

	return s.userRepository.Save(user)
}
