package domain

import (
	"crypto/rsa"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"time"
)

type AuthTokenService struct {
	privateKey *rsa.PrivateKey
}

func NewAuthTokenService(privateKey *rsa.PrivateKey) *AuthTokenService {
	return &AuthTokenService{
		privateKey: privateKey,
	}
}

func (s *AuthTokenService) PublicKey() *rsa.PublicKey {
	return &s.privateKey.PublicKey
}

func (s *AuthTokenService) GenerateAuthToken(user *User) *AuthToken {
	now := time.Now()
	accessToken := jwt.NewWithClaims(jwt.SigningMethodRS256, &CustomClaim{
		user.Id,
		jwt.StandardClaims{
			Id:        uuid.NewString(),
			Subject:   user.Login(),
			Issuer:    "auth-service",
			IssuedAt:  now.Unix(),
			ExpiresAt: now.Add(15 * time.Minute).Unix(),
		},
	})
	refreshToken := jwt.NewWithClaims(jwt.SigningMethodRS256, &CustomClaim{
		user.Id,
		jwt.StandardClaims{
			ExpiresAt: now.Add(1 * time.Hour).Unix(),
			Id:        uuid.NewString(),
			IssuedAt:  now.Unix(),
			Issuer:    "auth-service",
			Subject:   user.Login(),
		},
	})
	accessTokenString, _ := accessToken.SignedString(s.privateKey)
	refreshTokenString, _ := refreshToken.SignedString(s.privateKey)

	return &AuthToken{
		AccessToken:  accessTokenString,
		RefreshToken: refreshTokenString,
	}
}

func (s *AuthTokenService) Decode(tokenString string) (*jwt.Token, error) {
	return jwt.ParseWithClaims(tokenString, &CustomClaim{}, func(token *jwt.Token) (interface{}, error) {
		return s.privateKey.Public(), nil
	})
}

func (s *AuthTokenService) GenerateFromRefresh(refreshToken *jwt.Token) *AuthToken {
	claims, _ := refreshToken.Claims.(*CustomClaim)
	user := &User{
		Id: claims.UserId,
		Credentials: Credentials{
			Login: claims.Subject,
		},
	}

	return s.GenerateAuthToken(user)
}
