package main

import (
	"auth/app/controller"
	"auth/domain"
	"auth/infrastructure/persistence"
	"auth/infrastructure/utils"
	"embed"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"log"
	"net/http"
	"os"
	"text/template"
)

//go:embed app/controller/templates/*.html
var fs embed.FS

func main() {
	r := gin.Default()
	templates, err := template.ParseFS(fs, "app/controller/templates/sign-in.html", "app/controller/templates/sign-up.html")
	if err != nil {
		log.Fatal(err)
	}

	privateKeyString := os.Getenv("PRIVATE_KEY")
	if privateKeyString == "" {
		log.Fatal(errors.New("private key not present"))
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKeyString))
	if err != nil {
		log.Fatal(err)
	}

	passwordEncoder := utils.NewBCryptPasswordEncoder()
	tokenService := domain.NewAuthTokenService(privateKey)

	userRepository := persistence.NewInMemoryUserRepository()
	userService := domain.NewUserService(userRepository, passwordEncoder, tokenService)
	authController := controller.NewAuthController(userService, templates, tokenService)
	userController := controller.NewUserController(userService)

	auth := r.Group("/auth")
	{
		auth.GET("/sign-in", authController.SingInForm)
		auth.POST("/sign-in", authController.SignIn)
		auth.GET("/sign-up", authController.SignUpForm)
		auth.POST("/sign-up", authController.SignUp)
		auth.Any("/check", authController.Auth)
		auth.Any("/refresh", authController.Refresh)
	}

	users := r.Group("/users", controller.AuthMiddleware(tokenService))
	{
		users.GET("/:id/profile", userController.GetUser)
		users.PATCH("/:id/profile", userController.UpdateUserName)
	}

	r.GET("/health", func(context *gin.Context) {
		context.Status(http.StatusOK)
	})

	log.Fatal(r.Run(":8080"))
}
