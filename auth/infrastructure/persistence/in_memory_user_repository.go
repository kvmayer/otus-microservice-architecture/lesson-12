package persistence

import (
	"auth/domain"
	"sync"
)

type InMemoryUserRepository struct {
	users map[int]domain.User
	mu    *sync.Mutex
}

func NewInMemoryUserRepository() domain.UserRepository {
	return &InMemoryUserRepository{
		users: map[int]domain.User{
			1: {
				Id:      1,
				Name:    "foo",
				Surname: "bar",
				Credentials: domain.Credentials{
					Login:    "foo@bar.com",
					Password: "$2a$10$kEu0OwOM/nFHvoXqYqPxvex27Vf4rGXSI/zMBHUCAHlxZBTaLrfvq",
				},
			},
		},
		mu: &sync.Mutex{},
	}
}

func (r *InMemoryUserRepository) Save(user *domain.User) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.users[user.Id] = *user

	return nil
}

func (r *InMemoryUserRepository) FindByLogin(login string) (*domain.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	for _, user := range r.users {
		if user.Credentials.Login == login {
			return &user, nil
		}
	}

	return nil, nil
}

func (r *InMemoryUserRepository) FindById(id int) (*domain.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	user, ok := r.users[id]
	if ok {
		return &user, nil
	}

	return nil, nil
}

func (r *InMemoryUserRepository) NextId() int {
	maxId := 1

	for id := range r.users {
		if id > maxId {
			maxId = id
		}
	}

	return maxId + 1
}

