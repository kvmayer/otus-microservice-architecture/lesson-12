package utils

import "golang.org/x/crypto/bcrypt"

type BCryptPasswordEncoder struct {
}

func NewBCryptPasswordEncoder() *BCryptPasswordEncoder {
	return &BCryptPasswordEncoder{}
}

func (e *BCryptPasswordEncoder) Encode(password string) string {
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	return string(hash)
}

func (e *BCryptPasswordEncoder) ComparePassword(hash, password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)) == nil
}
