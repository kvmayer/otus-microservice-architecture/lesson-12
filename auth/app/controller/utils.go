package controller

import (
	"auth/domain"
	"github.com/gin-gonic/gin"
)

func requestContext(ctx *gin.Context) domain.RequestContext {
	value, ok := ctx.Get("request-context")
	if !ok {
		return domain.RequestContext{}
	}

	requestContext, ok := value.(domain.RequestContext)
	if !ok {
		return domain.RequestContext{}
	}

	return requestContext
}
