package controller

import (
	"auth/domain"
	"github.com/gin-gonic/gin"
	"net/http"
)

func handleError(ctx *gin.Context, err error) {
	code := http.StatusInternalServerError
	message := "internal server error"

	switch err {
	case domain.InvalidPassword:
		code = http.StatusBadRequest
		message = "invalid password"
	case domain.UserNotExists:
		code = http.StatusBadRequest
		message = "user not exists"
	case domain.UserAlreadyExists:
		code = http.StatusBadRequest
		message = "user already exists"
	case domain.InvalidRetryPassword:
		code = http.StatusBadRequest
		message = "invalid retry password"
	case domain.PermissionDeny:
		code = http.StatusForbidden
		message = "forbidden"
	}


	if ctx.GetHeader("Accept") == "application/json" {
		ctx.AbortWithStatusJSON(code, gin.H{
			"error": message,
		})
		return
	}

	ctx.AbortWithStatus(code)
}
