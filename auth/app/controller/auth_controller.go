package controller

import (
	"auth/domain"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"log"
	"net/http"
	"strconv"
	"text/template"
)

type AuthController struct {
	userService      *domain.UserService
	templates        *template.Template
	authTokenService *domain.AuthTokenService
}

func NewAuthController(userService *domain.UserService, templates *template.Template, authTokenService *domain.AuthTokenService) *AuthController {
	return &AuthController{userService: userService, templates: templates, authTokenService: authTokenService}
}

func (c *AuthController) SingInForm(ctx *gin.Context) {
	if err := c.templates.ExecuteTemplate(ctx.Writer, "sign-in.html", nil); err != nil {
		log.Println(err)
	}
}

func (c *AuthController) SignIn(ctx *gin.Context) {
	var dto domain.SignInDto
	if err := ctx.Bind(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	token, err := c.userService.UserSignIn(&dto)
	if err != nil {
		handleError(ctx, err)
		return
	}

	ctx.SetCookie("access-token", token.AccessToken, 0, "/", "", false, true)
	ctx.SetCookie("refresh-token", token.RefreshToken, 0, "/", "", false, true)

	ctx.Redirect(http.StatusFound, "/")
}

func (c *AuthController) SignUpForm(ctx *gin.Context) {
	if err := c.templates.ExecuteTemplate(ctx.Writer, "sign-up.html", nil); err != nil {
		log.Println(err)
	}
}

func (c *AuthController) SignUp(ctx *gin.Context) {
	var dto domain.SignUpDto
	if err := ctx.Bind(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	if err := c.userService.UserSingUp(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	ctx.Redirect(http.StatusFound, "/auth/sign-in")
}

func (c *AuthController) Auth(ctx *gin.Context) {
	var tokenString string

	if cookie, err := ctx.Cookie("access-token"); err == nil {
		tokenString = cookie
	}

	token, err := c.authTokenService.Decode(tokenString)
	if err != nil {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	if !token.Valid {
		ctx.AbortWithStatus(http.StatusForbidden)
		return
	}

	claims, _ := token.Claims.(*domain.CustomClaim)

	ctx.Header("x-user-id", strconv.Itoa(claims.UserId))
	ctx.Header("x-username", claims.Subject)
	ctx.Header("x-auth-token", tokenString)
	ctx.AbortWithStatus(http.StatusOK)
	return
}

func (c *AuthController) Refresh(ctx *gin.Context) {
	var tokenString string

	if cookie, err := ctx.Cookie("refresh-token"); err == nil {
		tokenString = cookie
	}

	token, err := c.authTokenService.Decode(tokenString)
	if token.Valid {
		auth := c.authTokenService.GenerateFromRefresh(token)
		ctx.SetCookie("access-token", auth.AccessToken, 0, "/", "", false, true)
		ctx.SetCookie("refresh-token", auth.RefreshToken, 0, "/", "", false, true)
		return
	}

	if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			// todo handle errors
		}
	}

	ctx.Status(http.StatusUnauthorized)
}
