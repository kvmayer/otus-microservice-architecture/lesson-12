package controller

import (
	"auth/domain"
	"github.com/gin-gonic/gin"
	"net/http"
)

func AuthMiddleware(authTokenService *domain.AuthTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var requestContext domain.RequestContext
		var tokenString string

		if cookie, err := ctx.Cookie("access-token"); err == nil {
			tokenString = cookie
		}

		if token, err := authTokenService.Decode(tokenString); err != nil || !token.Valid {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		} else {
			claims, _ := token.Claims.(*domain.CustomClaim)
			requestContext.UserId = claims.UserId
		}

		ctx.Set("request-context", requestContext)
		ctx.Next()
	}
}
