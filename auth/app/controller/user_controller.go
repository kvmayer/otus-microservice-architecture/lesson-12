package controller

import (
	"auth/app/controller/view"
	"auth/domain"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserController struct {
	userService *domain.UserService
}

func NewUserController(userService *domain.UserService) *UserController {
	return &UserController{userService: userService}
}

func (c *UserController) GetUser(ctx *gin.Context) {
	var dto domain.GetOwnedProfileDto
	if err := ctx.BindUri(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	user, err := c.userService.GetOwnedProfile(requestContext(ctx), &dto)
	if err != nil {
		handleError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, view.UserViewFromUser(user))
}

func (c *UserController) UpdateUserName(ctx *gin.Context) {
	var dto domain.UpdateOwnedProfile
	_ : ctx.BindUri(&dto)

	if err := ctx.Bind(&dto); err != nil {
		handleError(ctx, err)
		return
	}

	if err := c.userService.UpdateOwnedUserName(requestContext(ctx), &dto); err != nil {
		handleError(ctx, err)
		return
	}

	ctx.AbortWithStatus(http.StatusOK)
}
