package view

import "auth/domain"

type UserView struct {
	Id      int
	Name    string
	Surname string
	Login   string
}

func UserViewFromUser(user *domain.User) *UserView {
	return &UserView{
		Id:      user.Id,
		Name:    user.Name,
		Surname: user.Surname,
		Login:   user.Credentials.Login,
	}
}
